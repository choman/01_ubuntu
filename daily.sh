#!/bin/bash

BASEURL="http://cdimage.ubuntu.com/daily-live/current"
OS_RELEASE="/etc/os-release"
DEBUG=false


oscheck() 
{
   [[ ! -f "${OS_RELEASE}" ]] && echo "OS not supported" && exit

   source ${OS_RELEASE}

   if [[ ! ${ID} =~ ubuntu ]]; then
      echo "OS not supported: $ID" && exit
   fi
}

dependencies() 
{
   echo "Checking dependencies..."
   [[ ! -f /usr/bin/zsync ]] && sudo apt install -y zsync

}

runtime_check() 
{
   [[ $(pwd) =~ 01_ubuntu ]] && echo "Please run init.py" && exit
}

getChecksum()
{
   echo "Retrieving digest files..."
   if [ -f "$1" ]; then 
      rm -v "${1}"
   fi

   wget -O /tmp/"${1}" "${BASEURL}/${1}"

   #egrep "desktop-amd64.iso|desktop-i386.iso" /tmp/$1 > $1
   grep -E "desktop-amd64.iso" /tmp/"${1}" > "${1}"
}

downloadISO()
{
   isos="${1}"

   echo "Downloading ISOs via zsync..."

   for iso in $isos; do
      if [[ ${iso} =~ iso.zsync ]]; then
         echo "  - ${iso}"
         ##time zsync ${BASEURL}/utopic-desktop-amd64.iso.zsync
         time zsync "${BASEURL}/${iso}"
      fi
   done

   if $DEBUG; then
      echo
      echo "***************************************"
      echo "$isos"
      echo "***************************************"
   fi
}


oscheck
dependencies
runtime_check

#
# kinda ugly, but hey.   Try to use curl in future.  possible python app
isos=$(wget -q -O - http://cdimage.ubuntu.com/daily-live/current/ |  grep -E iso.zsync |  grep -o '<a href=['"'"'"][^"'"'"']*['"'"'"]' |   sed -e 's/^<a href=["'"'"']//' -e 's/["'"'"']$//')


##a=`curl --silent $BASEURL/ | grep zsync | grep -v mac`
##
##print a

echo "$isos"

downloadISO "$isos"
getChecksum "SHA256SUMS"
