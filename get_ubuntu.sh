#!/bin/bash

VERS=$(basename $(pwd) | cut -d_ -f1)

declare -A DIGESTS=(
    ["SHA256SUMS"]="sha256"
    ["SHA256SUMS.gpg"]="sha256.gpg"
  )


URLS=(
    "http://releases.ubuntu.com/${VERS}/ubuntu-${VERS}-desktop-amd64.iso"
    "http://releases.ubuntu.com/${VERS}/ubuntu-${VERS}-server-amd64.iso"
    "http://releases.ubuntu.com/${VERS}/ubuntu-${VERS}-live-server-amd64.iso"
    "http://releases.ubuntu.com/${VERS}/ubuntu-${VERS}-live-server-amd64.img"
    "http://cdimage.ubuntu.com/kubuntu/releases/${VERS}/release/kubuntu-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/lubuntu/releases/${VERS}/release/lubuntu-${VERS}-alternate-amd64.iso"
    "http://cdimage.ubuntu.com/lubuntu/releases/${VERS}/release/lubuntu-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/ubuntu-budgie/releases/${VERS}/release/ubuntu-budgie-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/ubuntucinnamon/releases/${VERS}/release/ubuntucinnamon-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/ubuntu-mate/releases/${VERS}/release/ubuntu-mate-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/ubuntu-unity/releases/${VERS}/release/ubuntu-unity-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/xubuntu/releases/${VERS}/release/xubuntu-${VERS}-desktop-amd64.iso"
    "http://cdimage.ubuntu.com/xubuntu/releases/${VERS}/release/xubuntu-${VERS}-minimal-amd64.iso"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-live-server-arm64.iso"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-live-server-s390x.iso"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-desktop-arm64+raspi.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-arm64+raspi.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-armhf+raspi.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-riscv64+icicle.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-riscv64+licheerv.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-riscv64+nezha.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-riscv64+unmatched.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-riscv64+visionfive2.img.xz"
    "https://cdimage.ubuntu.com/ubuntu/releases/${VERS}/release/ubuntu-${VERS}-preinstalled-server-riscv64+visionfive.img.xz"
    )

for url in ${URLS[@]}; do
	aria2c -c -x 8 --conditional-get=true $url

	base=$(dirname $url)
        fname=$(basename $url)

	for digest in "${!DIGESTS[@]}"; do

            if [[ $fname =~ .*desktop*. ]]; then
               outfile=${fname/-desktop-amd64.iso/.${DIGESTS[$digest]}}
	       aria2c -c -x 8 --conditional-get=true -o $outfile ${base}/$digest
	       sed -i -e '/i386/s/^[0-9a-z]/##/' $outfile
	       sed -i -e '/power/d' $outfile
	       sed -i -e "/${VERS}/!d" $outfile
	    fi

	done
done
