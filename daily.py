#!/bin/bash

BASEURL="http://cdimage.ubuntu.com/daily-live/current"

getChecksum ()
{
   if [ -f "$1" ]; then 
      rm -v $1
   fi

   wget ${BASEURL}/$1

   egrep "utopic-desktop-amd64.iso|utopic-desktop-i386.iso" $1 > /tmp/out
   cp /tmp/out $1
}

time zsync ${BASEURL}/utopic-desktop-amd64.iso.zsync
time zsync ${BASEURL}/utopic-desktop-i386.iso.zsync


getChecksum "MD5SUMS"
getChecksum "SHA256SUMS"

